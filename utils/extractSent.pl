#!/usr/bin/env perl

open SENTS_NUM, shift or die "$!\n";
open SENTS, shift or die "$!\n";

@all_sents = <SENTS>;

foreach (<SENTS_NUM>) {
	print $all_sents[$_-1];
}

close SENTS_NUM;
close SENTS;
