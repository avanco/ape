#!/usr/bin/env perl

#Este script cria um arquivo apenas com as sentencas definidas
#pelo arquivo passado como segundo parametro.
#As sentencas sao extraidas do arquivo passado como primeiro parametro

die "\nusage:\n\n\t$0 <arquivo com sentencas> <arquivo com nros das sentencas>\n\n" .
	"\n\tO arquivo com nros das sentencas deve estar no seguinte formato:\n\n" .
	"\tn1\n\tn2\n\tn3\n\t...\n\tnm\n\n" .
	"\tCada linha corresponde ao nro de uma sentenca\n\n"
if ($#ARGV != 1);

open SYS_TAG, "<", $ARGV[0] or die "$!\n";
open S_NUMBERS, "<", $ARGV[1] or die "$!\n";

@s_numbers = <S_NUMBERS>;
$s_number = shift @s_numbers;
foreach (<SYS_TAG>) {
	last if not $s_number;
	chomp $s_number;
	if (/^<s\sid=\"Sys_test\.$s_number.*/) {
		push @s, $_;
		$s_number = shift @s_numbers;
	}
}
print @s;
