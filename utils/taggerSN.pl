#!/usr/bin/env perl

use Data::Dumper;

open ONLY_SN_TAGS, $ARGV[0] or die "$!\n";
open ONLY_MORF_TAGS, $ARGV[1] or die "$!\n";

$sn = <ONLY_SN_TAGS>;
$morf = <ONLY_MORF_TAGS>;

@words = split ("<", $morf);
@words = grep { $_ !~ m%^/w>%  } @words;
shift @words;
$header_sentence = shift @words;
$header_sentence = "<".$header_sentence;

foreach (@words) {
	$_ = "<".$_."</w>";
}
$words[-1] = "</s>";

$idx = 0;
foreach (split (" ", $sn)) {
	$words[$idx] = $1.$words[$idx] if $_ =~ m%((<s[n|p]>)+)[,;\.\(\)0-9A-Za-zÀ-ú_]+%;
	$words[$idx] = $words[$idx].$1 if $_ =~ m%[,;\.\(\)0-9A-Za-zÀ-ú_]+((</s[n|p]>)+)%;
	++$idx;
}
unshift (@words, $header_sentence);
#print Dumper @words;

print join (' ',@words);
