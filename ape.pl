#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;
use List::Util qw(first);
use File::Basename qw(dirname);
use Cwd  qw(abs_path);
use lib dirname(abs_path $0).'/lib';
use H::GeraSuperficial qw(gera_superficial);

# quebra um scope (scalar) em um array contendo suas words
sub prepare_data {
	my $scope = shift;
	my @words = ();
	push @words,$& while $scope =~ m%<w[^<]*</w>%g;
	@words;
}

# in: scope com os tokens
# out: token nucleo do SN
# nucleo pode ter pos = n(substantivo), prn(pronome), np(nome proprio)
sub get_head { #TODO: decidir entre possiveis nucleos ?
	my @head = grep { $_ =~ /pos=\"\bn\b|\b[^\+]prn\b|\bnp\b\"/ } @{shift;};
	shift @head;
}

# in: head, scope
sub nominal_rules {
	my ($head, $ref_scope) = @_; my @scope = @{$ref_scope};
	@scope = grep { $_ !~ $head } @scope; # tira o nucleo de scope
	# regras: concordancia nominal
	my @nominal_scope = grep { $_ =~ /pos=\"\badj\b|\bdet\b|\bpr\+det\b|\bnum\b|\bpr\+prn\b\"/ } @scope;
	my $change_g_n = 0; # flag 1 indica que existe erro ou de genero ou de numero
	my $new_tok = undef; # forma correta do token
	foreach my $tok (@nominal_scope) {
		# DEBUG
		print "tok: $tok\n";
		print "head: $head\n";
		### DEBUG
		my ($head_gender, $head_number) = ($1, $2) if ($head =~ /.*gender=\"(\w+)\"\snumber=\"(\w+)\"/);
		my $tok_id = $1 if ($tok =~ /id=\"(\d+)\"/);
		my ($tok_gender, $tok_number) = ($1, $2) if ($tok =~ /.*gender=\"(\w+)\"\snumber=\"(\w+)\"/);
		# rule 1
		if ($tok_gender && $head_gender ne "mf" && $tok_gender ne "mf" && $tok_gender ne $head_gender) {
			$tok_gender = $head_gender;
			$change_g_n = 1;
		}
		# rule 2
		if ($tok_number && $head_number ne "sp" && $tok_number ne "sp" && $tok_number ne $head_number) {
			$tok_number = $head_number;
			$change_g_n = 1;
		}
		if ($change_g_n) {
			my $tok_lemma = $1 if ($tok =~ /.*lemma=\"([A-Za-zÀ-ú_]+)\".*/);
			my $tok_pos = $1 if ($tok =~ /pos=\"([\w|\+]+)\"/);
			if ($tok_pos eq "adj") {
				$new_tok = gera_superficial("lemma=$tok_lemma", "pos=adj", "number=$tok_number", "gender=$tok_gender");
			}
			elsif ($tok_pos eq "det") {
				my $tok_type = $1 if ($tok =~ /.*type=\"([A-Za-zÀ-ú_]+)\".*/);
				$new_tok = gera_superficial("lemma=$tok_lemma", "pos=det", "gender=$tok_gender", "number=$tok_number", "type=$tok_type");
			}
			elsif ($tok_pos eq "pr+det") {
				print "\nNao consegui gerar forma superficial de pr+det: CORRIGIR PALAVRA id=$tok_id\n\n";
			}
			elsif ($tok_pos eq "pr+prn") {
				print "\nNao consegui gerar forma superficial de pr+prn: CORRIGIR PALAVRA id=$tok_id\n\n";
			}
			else { # numeral, veja linha 31
				$new_tok = gera_superficial("lemma=$tok_lemma", "pos=num", "gender=$tok_gender", "number=$tok_number");
			}
			$change_g_n = 0;
		}
		print "\nPALAVRA CORRIGIDA (id=$tok_id): $new_tok\n" if ($new_tok);
		$new_tok = undef;
	}
}

# recupera apenas SN com palavras em seu interior, sem SP: <sn> w+ </sn>
# apenas isso jah permite concordar DET e MODs com o nucleo do SN
sub extract_closed_sns {
	my $sentnece = shift;
	my @sns = ();
	push @sns, $& while $sentnece =~ m%<sn>(<w[^<]*</w>(\s*))*([^<]*)</sn>%g;
	@sns;
}

# recupera um SP que possui SNs em seu interior
# permite fazer concordancia entre PREP e PREP+DET com os SNs
sub extract_sn_in_sp {
	my @sns = @{$_[0]};
	my $sentence = $_[1];
	my @sn_in_sp = ();
	my $idx = 0;
	#my @remove_sns = (); # SNs serao tratados no escopo de <sp>..</sp>
	foreach my $sn (@{$_[0]}) {
		my $q_sn = quotemeta($sn);
		#push @sn_in_sp,$& if $sentence =~ m%<sp>((<w[^<]*</w>(\s*))|(<sn>))+\s*$q_sn\s*(<w[^<]*</w>(\s*))*\s*</?sp>%;
		if ( $sentence =~ m%<sp>((<w[^<]*</w>(\s*))|(<sn>))+\s*$q_sn\s*(<w[^<]*</w>(\s*))*\s*</?sp>% ) {
			push @sn_in_sp, $&;
			${$_[0]}[$idx] = undef;
		}
		++$idx;
	}
	# o array de SNs simples (closed_sns) eh alterado para conter apenas SN fora de SP
	@{$_[0]} = grep { defined $_ } @{$_[0]};
	@sn_in_sp;
}

sub main {
	# SYS_TAG_SN --> resultado da TA com etiquetas e SNs e SPs
	open SYS_TAG_SN, $ARGV[0] or die "$!\n";
	foreach my $s (<SYS_TAG_SN>) {
		my $sentence_n = $1 if ($s =~ /Sys_test.(\d+)/);
		print "SENTENCE $sentence_n\n";
		my @closed_sns = &extract_closed_sns($s);
		my @sn_in_sp = &extract_sn_in_sp(\@closed_sns, $s);
		foreach (@closed_sns) {
			my @sn = &prepare_data($_);
# DEBUG - ver todos scopes criados
#print Dumper @sn;
			if ($#sn > 0) { # se o SN tiver algo alem do nucleo
				my $head = &get_head(\@sn);
				&nominal_rules($head, \@sn) if defined $head;
				print "#################\n";
			}
		}
		foreach (@sn_in_sp) {
			my @sn_sp = &prepare_data($_);
# DEBUG - ver todos scopes criados <sp> ... <sn>? ...</sn>! ...<sn> </sn> ... </?sp>
#print Dumper @sn_sp;
			my $head = &get_head(\@sn_sp);
			&nominal_rules($head, \@sn_sp) if defined $head;
			print "#################\n";
		}
	}
	close SYS_TAG_SN;
}

&main;
